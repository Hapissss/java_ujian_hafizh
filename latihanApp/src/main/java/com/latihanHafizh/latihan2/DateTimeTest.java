package com.latihanHafizh.latihan2;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Locale;

public class DateTimeTest {
    // public void showDate(){
    //     LocalDate tanggal = LocalDate.now();
    //     System.out.println(tanggal);

    //     LocalTime jam = LocalTime.now();
    //     System.out.println(jam);

    //     LocalDateTime tanggalWaktu = LocalDateTime.now();
    //     System.out.println(tanggalWaktu);

    //     DateTimeFormatter formatTanggal = DateTimeFormatter.ofPattern("dd-MM-yy");
    //     String tanggalBaru = tanggal.format(formatTanggal);
    //     System.out.println(tanggalBaru);

    //     DateTimeFormatter formatWaktu = DateTimeFormatter.ofPattern("HH:mm:ss");
    //     String waktuBaru = jam.format(formatWaktu);
    //     System.out.println(waktuBaru);
    // }

    public void tanggalLahirBaru(){
        LocalDate tanggalSekarang = LocalDate.now();
        String tanggalLahir = "04-12-2001";
        
        
        DateTimeFormatter formatHitung = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        DateTimeFormatter formatInput = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        DateTimeFormatter formatTampilan = DateTimeFormatter.ofPattern("EEEE dd-MM-yyyy", new Locale("id", "ID"));
        
        // Untuk Menghitung
        String tanggalSekarangHitung = tanggalSekarang.format(formatHitung);
        LocalDate tanggalLahirHitung = LocalDate.parse(tanggalLahir, formatInput);
        LocalDate tanggalSekarangSiapHitung = LocalDate.parse(tanggalSekarangHitung, formatHitung);
        long diantara = ChronoUnit.DAYS.between(tanggalLahirHitung, tanggalSekarang);
        // End Untuk Menghitung
        
        // Untuk Menampilkan
        LocalDate tanggalLahirBaru = LocalDate.parse(tanggalLahir, formatInput);        

        // Output
        System.out.println("Tanggal Sekarang Mu: " + tanggalSekarang.format(formatTampilan));
        System.out.println("Tanggal Lahir Mu: " + tanggalLahirBaru.format(formatTampilan));
        System.out.println("Hari-hari Yang Telah Lewat: " + diantara);

    }
}