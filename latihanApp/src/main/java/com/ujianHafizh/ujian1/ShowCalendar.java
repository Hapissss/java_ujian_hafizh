package com.ujianHafizh.ujian1;

public class ShowCalendar {
    public void showCalendar(){
        int tahun = 1998;    
        int mulaiHarike = 3;
        int spaces = mulaiHarike;

        String[] bulan = {
                "",                               
                "Januari", "Februari", "Maret",
                "April", "Mei", "Juni",
                "Juli", "Augstus", "September",
                "Oktober", "November", "Desember"
            };

            int[] jumlahHari = {
                0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
            };

            for (int b = 1; b <= 12; b++) {

            if  ((((tahun % 4 == 0) && (tahun % 100 != 0)) ||  (tahun % 400 == 0)) && b == 2)
                jumlahHari[b] = 29;


            System.out.println("==========Bulan "+ bulan[b] + "==========");

            System.out.println("   M    S    S    R    K    J    S");

               spaces = (jumlahHari[b-1] + spaces)%7;

            for (int i = 0; i < spaces; i++)
                System.out.print("     ");
            for (int i = 1; i <= jumlahHari[b]; i++) {
                System.out.printf(" %3d ", i);
                if (((i + spaces) % 7 == 0) || (i == jumlahHari[b])) System.out.println();
            }

            System.out.println();
        }
    }
}