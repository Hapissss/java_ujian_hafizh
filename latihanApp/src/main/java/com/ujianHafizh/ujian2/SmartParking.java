package com.ujianHafizh.ujian2;

public class SmartParking {
    String kendaraan;
    String tipe;
    String waktuMasuk;
    String waktuKeluar;
    String parkingId;

    String tahun;
    String bulan;
    String tanggal;
    int random;

    public SmartParking(String kendaraan, String tipe){
        this.kendaraan= kendaraan;
        this.tipe = tipe;
    }
    
    public void setVehicle(String kendaraan){
        this.kendaraan = kendaraan;
    }
    public void setType(String tipe){
        this.tipe = tipe;
    }

    String getVehicle(){
        return this.kendaraan;
    }
    String getTipe(){
        return this.tipe;
    }

    public void setParkingId(String waktuMasuk){
        this.waktuMasuk = waktuMasuk;
        this.tahun = this.waktuMasuk.substring(2, 4);
        this.bulan = this.waktuMasuk.substring(5, 7);
        this.tanggal = this.waktuMasuk.substring(8, 10);
        this.parkingId = "P" + tahun + bulan + tanggal;
    }

    String getParkingId(){
        return this.parkingId;
    }

    
}