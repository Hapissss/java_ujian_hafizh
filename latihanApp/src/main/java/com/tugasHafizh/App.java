package com.tugasHafizh;
import java.util.ArrayList;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );

        ArrayList<String> furniture = new ArrayList<>();
        furniture.add("Kursi");
        furniture.add("Lemari");
        furniture.add("Meja");

        ArrayList<String> furnitureKamar = new ArrayList<>();
        furnitureKamar.add("Meja");
        furnitureKamar.add("Tempat Tidur");

        ArrayList<String> newFurniture = new ArrayList<String>(furniture);
        newFurniture.addAll(furnitureKamar);

        ArrayList<String> substract = new ArrayList<String>(furniture);
        substract.retainAll(furnitureKamar);

        newFurniture.removeAll(substract);            

	    furniture.removeAll(furniture);
        furniture.addAll(newFurniture);
        System.out.println(furniture);

        // DIBANTU DENGAN TUGAS FELIX
        // Alur sudah mengerti kak.
    }
}