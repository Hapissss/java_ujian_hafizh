package com.ujianHafizh.ujian2;

public class Kendaraan {
    String waktuMasuk;
    String waktuKeluar;
    int jamPertama;
    int jamSeterusnya;
    int jamMasuk;
    int jamKeluar;
    SmartParking kartu;

    public void setParking(SmartParking kartu){
        this.kartu = kartu;
    }

    public void setJamMasuk(String waktuMasuk){
        this.jamMasuk = Integer.parseInt(waktuMasuk.substring(11, 13));
    }
    public void setJamKeluar(String waktuKeluar){
        this.jamKeluar = Integer.parseInt(waktuKeluar.substring(11, 13));
    }

    String getWaktuMasuk(){
        return this.waktuMasuk;
    }

    String getWaktuKeluar(){
        return this.waktuKeluar;
    }

    public int getTotalBayar(){
        return (this.jamKeluar-this.jamMasuk)*this.jamSeterusnya-this.jamSeterusnya+this.jamPertama;
    }
}