package com.latihanHafizh.latihan1;

public class KartuPelajar {
    private int nis;
    private String nama;
    private int kelas;
    private String jenjang;

    // Contstructure Start
    public KartuPelajar(int nis, String nama, int kelas){
        this.nis = nis;
        this.nama = nama;
        this.kelas = kelas;
        
        if (this.kelas < 7 && this.kelas > 0) {
            this.jenjang = "SD";
        }

        else if(this.kelas > 6 && this.kelas < 10){
            this.jenjang = "SMP";
        }

        else if(this.kelas > 9 && this.kelas < 13){
            this.jenjang = "SMA";
        }

        else{
            throw new ArithmeticException("Kelas Tidak Valid");
        }
    }
    // Constructure End

    // Setter Start
    public void setNama(String nama){
        this.nama = nama;
    }

    public void setKelas(int kelas){
        this.kelas = kelas;
    }

    public void setJenjang(String jenjang){
        this.jenjang = jenjang;
    }
    // Setter End
    
    // Getter Start
    int getNIS(){
        return this.nis;
    }

    String getNama(){
        return this.nama;
    }

    int getKelas(){
        return this.kelas;
    }

    String getJenjang(){
        return this.jenjang;
    }
    // Getter End
}