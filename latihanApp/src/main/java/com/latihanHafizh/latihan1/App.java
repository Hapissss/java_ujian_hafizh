package com.latihanHafizh.latihan1;
import java.util.ArrayList;

public class App{
    public static void main(String[] args) {
        SD anakSD = new SD();
        KartuPelajar kartuSD = new KartuPelajar(1, "Naruto", 3);
        anakSD.setKartuPelajar(kartuSD);
    
        SMP anakSMP = new SMP();
        KartuPelajar kartuSMP = new KartuPelajar(2, "Pinta", 8);
        anakSMP.setKartuPelajar(kartuSMP);
    
        SMA anakSMA = new SMA();
        KartuPelajar kartuSMA = new KartuPelajar(3, "Hana", 12);
        anakSMA.setKartuPelajar(kartuSMA);

        ArrayList<Siswa> Murid = new ArrayList<Siswa>();
        Murid.add(anakSD);
        Murid.add(anakSMP);
        Murid.add(anakSMA);

        for(int i=0 ; i<Murid.size() ; i++){
        System.out.println("====================" + "\n");
        System.out.println("NIS: " + Murid.get(i).getKartu().getNIS() + "\n" + 
                            "Nama: " + Murid.get(i).getKartu().getNama() + "\n" + 
                            "Kelas: " + Murid.get(i).getKartu().getKelas() + "\n" + 
                            "Jenjang: " + Murid.get(i).getKartu().getJenjang() + "\n" + 
                            "Uang Jajan: " + Murid.get(i).getTotalJajan() + "\n");
        System.out.println("====================" + "\n");
        }
    }
}